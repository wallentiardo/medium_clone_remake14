<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;

class ArticleController extends Controller
{
    public function index(){
        $article = Article::all();
        return view('admin.article.index', ["articles" => $article]);
    }

    public function create(){
        return view('admin.article.create');
    }

    public function store(Request $request){
        $article = Article::create($request->all());
        return redirect('/admin/article');
    }

    public function show($id){
        $article = Article::find($id);
        return view('admin.article.show', ["articles" => $article]);
    }

    public function edit($id){
        $article = Article::find($id);
        return view('admin.article.update', ["articles" => $article]);
    }

    public function update(Request $request, $id){
        $article = Article::find($id);
        $article->update($request->all());
        return redirect('/admin/article');     
    }

    public function destroy($id){
        Article::destroy($id);
        return redirect('/admin/article');
    }
}
