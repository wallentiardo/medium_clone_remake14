<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function index(){
        $category = Category::all();
        return view('admin.category.index', ["categories" => $category]);
    }

    public function create(){
        return view('admin.category.create');
    }

    public function store(Request $request){
        $category = Category::create($request->all());
        return redirect('/admin/category');
    }

    public function show($id){
        $category = Category::find($id);
        return view('admin.category.show', ["categories" => $category]);
    }

    public function edit($id){
        $category = Category::find($id);
        return view('admin.category.update', ["categories" => $category]);
    }

    public function update(Request $request, $id){
        $category = Category::find($id);
        $category->update($request->all());
        return redirect('/admin/category');     
    }

    public function destroy($id){
        Category::destroy($id);
        return redirect('/admin/category');
    }
}
