<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;

class ProfileController extends Controller
{
    public function index(){
        $profile = Profile::all();
        return view('admin.profile.index', ["profiles" => $profile]);
    }

    public function create(){
        return view('admin.profile.create');
    }

    public function store(Request $request){
        $profile = Profile::create($request->all());
        return redirect('/admin/profile');
    }

    public function show($id){
        $profile = Profile::find($id);
        return view('admin.profile.show', ["profiles" => $profile]);
    }

    public function edit($id){
        $profile = Profile::find($id);
        return view('admin.profile.update', ["profiles" => $profile]);
    }

    public function update(Request $request, $id){
        $profile = Profile::find($id);
        $profile->update($request->all());
        return redirect('/admin/profile');     
    }

    public function destroy($id){
        Profile::destroy($id);
        return redirect('/admin/profile');
    }
}
