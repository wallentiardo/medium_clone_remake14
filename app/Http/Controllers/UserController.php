<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function index(){
        $user = User::all();
        return view('admin.user.index', ["users" => $user]);
    }

    public function create(){
        return view('admin.user.create');
    }

    public function store(Request $request){
        $user = User::create($request->all());
        return redirect('/admin/user');
    }

    public function show($id){
        $user = User::find($id);
        return view('admin.user.show', ["users" => $user]);
    }

    public function edit($id){
        $user = User::find($id);
        return view('admin.user.update', ["users" => $user]);
    }

    public function update(Request $request, $id){
        $user = User::find($id);
        $user->update($request->all());
        return redirect('/admin/user');     
    }

    public function destroy($id){
        User::destroy($id);
        return redirect('/admin/user');
    }
}
