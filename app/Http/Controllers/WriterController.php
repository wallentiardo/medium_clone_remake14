<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;

class WriterController extends Controller
{
    public function index(){
        $article = Article::all();
        return view('writer.article.index', ["articles" => $article]);
    }

    public function show($id){
        $article = Article::find($id);
        return view('writer.article.show', ["articles" => $article]);
    }

    public function create(){
        return view('writer.article.create');
    }

    public function store(Request $request){
        $article = Article::create($request->all());
        return redirect('/writer/article');
    }
}
