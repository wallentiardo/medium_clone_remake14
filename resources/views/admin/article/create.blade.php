@extends('layouts.master')

@section('page_header')
    Add New Article
@endsection

@section('page_title')
    Add New Article
@endsection

@section('content')
    <form action="/admin/article" method="POST">
        @csrf
        <div class="form-group">
            <label for="name">Judul :</label>
            <input type="text" class="form-control" placeholder="Enter Title" name="judul" id="judul">
        </div>
        <div class="form-group">
            <label for="price">Isi :</label>
            <textarea id="my-editor" name="isi" class="form-control">{!! old('content', 'Article Content Here') !!}</textarea>
        </div>
        <div class="form-group">
            <label for="price">Id Penulis :</label>
            <input type="text" class="form-control" placeholder="Enter Writer's Id" name="profile_id" id="profile_id">
        </div>
        <div class="form-group">
            <label for="price">Kategori :</label>
            <input type="text" class="form-control" placeholder="Enter Category Id" name="category_id" id="category_id">
        </div>
        <button type="submit" class="btn btn-primary">Add</button>
        <a class="btn btn-danger" href="/admin/article" role="button">Cancel</a>
    </form>
@endsection

@push('scripts')
    <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
    <script>
        var options = {
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
        };
        CKEDITOR.replace('my-editor', options);
    </script>
@endpush