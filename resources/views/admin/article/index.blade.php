@extends('layouts.master')

@section('page_header')
    List Article
@endsection

@section('page_title')
    List Article
@endsection

@section('content')
    <a class="btn btn-primary" href="/admin/article/create" role="button">Add New Article</a>
    <br><br>
    
    <table class="table table-dark table-striped">
        <thead>
        <tr class="text-center">
            <th>No</th>
            <th>Judul</th>
            <th>Penulis</th>
            <th>Kategori</th>
            <th>Created</th>
            <th>Updated</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
            @foreach($articles as $k => $a)
                <tr class="text-center">
                    <td>{{ $k + 1 }}</td>
                    <td>{{ $a -> judul }}</td>
                    <td>{{ $a -> profile -> nama }}</td>
                    <td>{{ $a -> category -> nama }}</td>
                    <td>{{ $a -> created_at }}</td>
                    <td>{{ $a -> updated_at }}</td>
                    <td><a class="btn btn-sm btn-success" href="/admin/article/{{$a -> id}}" role="button"><i class="fa fa-eye"></i> </a></td>
                    <td><a class="btn btn-sm btn-warning" href="/admin/article/{{$a->id}}/edit" role="button"><i class="fa fa-edit"></a></td>
                    <td>
                        <form action="{{route('article.destroy', ['article' => $a->id])}}" method="POST" style="display:inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-sm btn-danger" value="Delete"><i class="fa fa-trash"></i></button> 
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <p>
    </p>
@endsection
