@extends('layouts.master')

@section('page_header')
    Article Detail
@endsection

@section('page_title')
    Article Detail
@endsection

@section('content')
    <div class="card bg-dark text-white ">
        <div class="card-header text-center">
            <h3>{{ $articles -> judul }}</h3>
        </div>
        <div class="card-body">
            <table class="table text-center">
                <tr>
                    <td colspan="3">{!! $articles -> isi !!}</td>
                </tr>
                <tr>
                    <td>Penulis</td>
                    <td></td>
                    <td>{{ $articles -> profile -> nama }}</td>
                </tr>
                <tr>
                    <td>Kategori</td>
                    <td></td>
                    <td>{{ $articles -> category -> nama }}</td>
                </tr>
                <tr>
                    <td>Created</td>
                    <td></td>
                    <td>{{ $articles -> created_at }}</td>
                </tr>
                <tr>
                    <td>Updated</td>
                    <td></td>
                    <td>{{ $articles -> updated_at }}</td>
                </tr>
            </table>       
        </div>
    </div>
    <a class="btn btn-danger" href="/admin/article" role="button">Back</a>
    



@endsection