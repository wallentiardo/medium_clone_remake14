@extends('layouts.master')

@section('page_header')
    Update User Profile
@endsection

@section('page_title')
    Update User Profile
@endsection

@section('content')
    <form action=" /admin/article/{{$articles->id}}" method="POST">
        {{csrf_field()}}
        {{method_field('PUT')}}
        <div class="form-group">
            <label for="name">Judul :</label>
            <input type="text" class="form-control" placeholder="Enter Title" name="judul" id="judul" value="{{$articles -> judul}}">
        </div>
        <div class="form-group">
            <label for="price">Isi :</label>
            <textarea id="my-editor" name="isi" class="form-control"> {{$articles -> isi}}</textarea>
        </div>
        <div class="form-group">
            <label for="price">Id Penulis :</label>
            <input type="text" class="form-control" placeholder="Enter Writer's Id" name="profile_id" id="profile_id" value="{{$articles -> profile_id}}">
        </div>
        <div class="form-group">
            <label for="price">Kategori :</label>
            <input type="text" class="form-control" placeholder="Enter Category Id" name="category_id" id="category_id" value="{{$articles -> category_id}}">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Update</button>
            <a class="btn btn-danger" href="/admin/article" role="button">Cancel</a>
        </div>
    </form>
@endsection
@push('scripts')
    <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
    <script>
        var options = {
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
        };
        CKEDITOR.replace('my-editor', options);
    </script>
@endpush