@extends('layouts.master')

@section('page_header')
    Add New Category
@endsection

@section('page_title')
    Add New Category
@endsection

@section('content')
    <form action="/admin/category" method="POST">
        @csrf
        <div class="form-group">
            <label for="name">Nama :</label>
            <input type="text" class="form-control" placeholder="Enter Category's Name" name="nama" id="nama">
        </div>
        <button type="submit" class="btn btn-primary">Add</button>
        <a class="btn btn-danger" href="/admin/category" role="button">Cancel</a>
    </form>
@endsection