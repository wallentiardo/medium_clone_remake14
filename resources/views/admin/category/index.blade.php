@extends('layouts.master')

@section('page_header')
    List Article Category
@endsection

@section('page_title')
    List Article Category
@endsection

@section('content')
    <a class="btn btn-primary" href="{{route('category.create')}}" role="button">Add New Category</a>
    <br><br>
    <table class="table table-dark table-striped center" style="width:350px;">
        <thead>
        <tr class="text-center">
            <th>No</th>
            <th>Kategori</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
            @foreach($categories as $k => $c)
                <tr class="text-center">
                    <td>{{ $k + 1 }}</td>
                    <td>{{ $c -> nama }}</td>
                    <td><a class="btn btn-sm btn-success" href="{{route('category.show', ['category' => $c->id])}}" role="button"><i class="fa fa-eye"></i> </a></td>
                    <td><a class="btn btn-sm btn-warning" href="{{route('category.edit', ['category' => $c->id])}}" role="button"><i class="fa fa-edit"></a></td>
                    <td>
                        <form action="{{route('category.destroy', ['category' => $c->id])}}" method="POST" style="display:inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-sm btn-danger" value="Delete"><i class="fa fa-trash"></i></button> 
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
