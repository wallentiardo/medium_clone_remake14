@extends('layouts.master')

@section('page_header')
    Article Related To {{$categories->nama}}
@endsection

@section('page_title')
    List Article
@endsection

@section('content')
    <table class="table table-dark table-striped">
        <thead>
            <tr class="text-center">
                <th>No</th>
                <th>Judul</th>
                <th>Penulis</th>
                <th>Kategori</th>
                <th>Created</th>
                <th>Updated</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($categories->article as $k => $a)
                <tr class="text-center">
                    <td>{{ $k + 1 }}</td>
                    <td>{{ $a -> judul }}</td>
                    <td>{{ $a -> profile -> nama }}</td>
                    <td>{{ $a -> category -> nama }}</td>
                    <td>{{ $a -> created_at }}</td>
                    <td>{{ $a -> updated_at }}</td>
                    <td><a class="btn btn-sm btn-success" href="{{route('article.show', ['article' => $a->id])}}" role="button"><i class="fa fa-eye"></i> </a></td>
                    <td><a class="btn btn-sm btn-warning" href="{{route('article.edit', ['article' => $a->id])}}" role="button"><i class="fa fa-edit"></a></td>
                    <td>
                        <form action="{{route('article.destroy', ['article' => $a->id])}}" method="POST" style="display:inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-sm btn-danger" value="Delete"><i class="fa fa-trash"></i></button> 
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <a class="btn btn-danger" href="/admin/category" role="button">Back</a>
@endsection
