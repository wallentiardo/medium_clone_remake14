@extends('layouts.master')

@section('page_header')
    Update Category
@endsection

@section('page_title')
    Update Category
@endsection

@section('content')
    <form action=" /admin/category/{{$categories->id}}" method="POST">
        {{csrf_field()}}
        {{method_field('PUT')}}
        <div class="form-group">
            <label for="name">Nama :</label>
            <input type="text" class="form-control" placeholder="Enter Category's Name" name="nama" id="nama" value="{{$categories -> nama}}">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Update</button>
            <a class="btn btn-danger" href="/admin/category" role="button">Cancel</a>
        </div>
    </form>
@endsection