@extends('layouts.master')

@section('page_header')
    Add New User Profile
@endsection

@section('page_title')
    Add New User Profile
@endsection

@section('content')
    <form action="/admin/profile" method="POST">
        @csrf
        <div class="form-group">
            <label for="name">Nama :</label>
            <input type="text" class="form-control" placeholder="Enter Name" name="nama" id="nama">
        </div>
        <div class="form-group">
            <label for="price">Email :</label>
            <input type="text" class="form-control" placeholder="Enter Email" name="email" id="email">
        </div>
        <div class="form-group">
            <label for="price">Birth Day :</label>
            <input type="text" class="form-control" placeholder="Enter Birth Day" name="birth" id="birth">
        </div>
        <div class="form-group">
            <label for="price">Telepon :</label>
            <input type="text" class="form-control" placeholder="Enter Phone Number" name="telp" id="telp">
        </div>
        <div class="form-group">
            <label for="price">Alamat :</label>
            <input type="text" class="form-control" placeholder="Enter Address" name="alamat" id="alamat">
        </div>
        <div class="form-group">
            <label for="price">User Id :</label>
            <input type="number" class="form-control" placeholder="Enter User Id" name="user_id" id="user_id">
        </div>
        <button type="submit" class="btn btn-primary">Add</button>
        <a class="btn btn-danger" href="/admin/profile" role="button">Cancel</a>
    </form>
@endsection