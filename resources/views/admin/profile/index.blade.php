@extends('layouts.master')

@section('page_header')
    List Profile User
@endsection

@section('page_title')
    List Profile User
@endsection

@section('content')
    <a class="btn btn-primary" href="{{route('profile.create')}}" role="button">Add New User Profile</a>
    <br><br>
    
    <table class="table table-dark table-striped">
        <thead>
        <tr class="text-center">
            <th>No</th>
            <th>Nama</th>
            <th>Email</th>
            <th>Birthday</th>
            <th>Telepon</th>
            <th>Alamat</th>
            <th>Username</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
            @foreach($profiles as $k => $p)
                <tr class="text-center">
                    <td>{{ $k + 1 }}</td>
                    <td>{{ $p -> nama }}</td>
                    <td>{{ $p -> email }}</td>
                    <td>{{ $p -> birth }}</td>
                    <td>{{ $p -> telp }}</td>
                    <td>{{ $p -> alamat }}</td>
                    <td> {{ $p -> user -> username }} </td>
                    <td><a class="btn btn-sm btn-success" href="{{route('profile.show', ['profile' => $p->id])}}" role="button"><i class="fa fa-eye"></i> </a></td>
                    <td><a class="btn btn-sm btn-warning" href="{{route('profile.edit', ['profile' => $p->id])}}" role="button"><i class="fa fa-edit"></a></td>
                    <td>
                        <form action="{{route('profile.destroy', ['profile' => $p->id])}}" method="POST" style="display:inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-sm btn-danger" value="Delete"><i class="fa fa-trash"></i></button> 
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <p>
    </p>
@endsection
