@extends('layouts.master')

@section('page_header')
    Detail User Profile
@endsection

@section('page_title')
    Detail User Profile
@endsection

@section('content')

    <div class="card bg-dark text-white ">
        <div class="card-header text-center">
            <h3>User Profile</h3>
        </div>
        <div class="card-body">
            <table class="table text-center">
                <tr>
                    <td>Nama</td>
                    <td></td>
                    <td>{{$profiles -> nama}}</td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td></td>
                    <td>{{$profiles -> email}}</td>
                </tr>
                <tr>
                    <td>Role</td>
                    <td></td>
                    <td>{{$profiles -> birth}}</td>
                </tr>
                <tr>
                    <td>Telepon</td>
                    <td></td>
                    <td>{{$profiles -> telp}}</td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td></td>
                    <td>{{$profiles -> alamat}}</td>
                </tr>
                <tr>
                    <td>User Id</td>
                    <td></td>
                    <td>{{$profiles -> user -> id}}</td>
                </tr>
                <tr>
                    <td>Username</td>
                    <td></td>
                    <td>{{$profiles -> user -> username}}</td>
                </tr>
                <tr>
                    <td>User Role</td>
                    <td></td>
                    <td>{{$profiles -> user -> role}}</td>
                </tr>
            </table>       
        </div>
    </div>
    <a class="btn btn-danger" href="/admin/user" role="button">Back</a>
    



@endsection