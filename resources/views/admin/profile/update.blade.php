@extends('layouts.master')

@section('page_header')
    Update User Profile
@endsection

@section('page_title')
    Update User Profile
@endsection

@section('content')
    <form action=" /admin/profile/{{$profiles->id}}" method="POST">
        {{csrf_field()}}
        {{method_field('PUT')}}
        <div class="form-group">
            <label for="name">Nama :</label>
            <input type="text" class="form-control" placeholder="Enter Name" name="nama" id="nama" value="{{$profiles -> nama}}">
        </div>
        <div class="form-group">
            <label for="price">Email :</label>
            <input type="text" class="form-control" placeholder="Enter Email" name="email" id="email" value="{{$profiles -> email}}">
        </div>
        <div class="form-group">
            <label for="price">Birth Day :</label>
            <input type="text" class="form-control" placeholder="Enter Birth Day" name="birth" id="birth" value="{{$profiles -> birth}}">
        </div>
        <div class="form-group">
            <label for="price">Telepon :</label>
            <input type="text" class="form-control" placeholder="Enter Phone Number" name="telp" id="telp" value="{{$profiles -> telp}}">
        </div>
        <div class="form-group">
            <label for="price">Alamat :</label>
            <input type="text" class="form-control" placeholder="Enter Address" name="alamat" id="alamat" value="{{$profiles -> alamat}}">
        </div>
        <div class="form-group">
            <label for="price">User Id :</label>
            <input type="number" class="form-control" placeholder="Enter User Id" name="user_id" id="user_id" value="{{$profiles -> user_id}}">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Update</button>
            <a class="btn btn-danger" href="/admin/profile" role="button">Cancel</a>
        </div>
    </form>
@endsection