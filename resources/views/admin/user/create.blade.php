@extends('layouts.master')

@section('page_header')
    Add New User Account
@endsection

@section('page_title')
    Add New User Account
@endsection

@section('content')
    <form action="/admin/user" method="POST">
        @csrf
        <div class="form-group">
            <label for="name">Username :</label>
            <input type="text" class="form-control" placeholder="Enter username" name="username" id="username">
        </div>
        <div class="form-group">
            <label for="name">Password :</label>
            <input type="password" class="form-control" placeholder="Enter password" name="password" id="password">
        </div>

        <div class="form-group">
            <label for="price">Role :</label>
            <input type="text" class="form-control" placeholder="Enter user's role" name="role" id="role">
        </div>
        <button type="submit" class="btn btn-primary">Add</button>
        <a class="btn btn-danger" href="/admin/user" role="button">Cancel</a>
    </form>
@endsection