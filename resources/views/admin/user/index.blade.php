@extends('layouts.master')

@section('page_header')
    List User Account
@endsection

@section('page_title')
    List User Account
@endsection

@section('content')
    <a class="btn btn-primary" href="{{route('user.create')}}" role="button">Add New User</a>
    <br><br>
    
    <table class="table table-dark table-striped">
        <thead>
        <tr class="text-center">
            <th>No</th>
            <th>Username</th>
            <th>Password</th>
            <th>Role</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
            @foreach($users as $k => $u)
                <tr class="text-center">
                    <td>{{ $k + 1 }}</td>
                    <td>{{ $u -> username }}</td>
                    <td>{{ $u -> password }}</td>
                    <td>{{ $u -> role }}</td>
                    <td><a class="btn btn-sm btn-success" href="{{route('user.show', ['user' => $u->id])}}" role="button"><i class="fa fa-eye"></i> </a></td>
                    <td><a class="btn btn-sm btn-warning" href="{{route('user.edit', ['user' => $u->id])}}" role="button"><i class="fa fa-edit"></a></td>
                    <td>
                        <form action="{{route('user.destroy', ['user' => $u->id])}}" method="POST" style="display:inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-sm btn-danger" value="Delete"><i class="fa fa-trash"></i></button> 
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <p>
    </p>
@endsection
