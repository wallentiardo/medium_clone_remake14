@extends('layouts.master')

@section('page_header')
    Detail User Account
@endsection

@section('page_title')
    Detail User Acoount
@endsection

@section('content')

    <div class="card bg-dark text-white ">
        <div class="card-header text-center">
            <h3>User Account</h3>
        </div>
        <div class="card-body">
            <table class="table text-center">
                <tr>
                    <td>Username</td>
                    <td></td>
                    <td>{{$users->username}}</td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td></td>
                    <td>{{$users->password}}</td>
                </tr>
                <tr>
                    <td>Role</td>
                    <td></td>
                    <td>{{$users->role}}</td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td></td>
                    <td>{{$users->profile->nama}}</td>
                </tr>
            </table>       
        </div>
    </div>
    <a class="btn btn-danger" href="/admin/user" role="button">Back</a>
    



@endsection