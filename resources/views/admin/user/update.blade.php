@extends('layouts.master')

@section('page_header')
    Update Item
@endsection

@section('page_title')
    Update Item
@endsection

@section('content')
    <form action=" /admin/user/{{$users->id}}" method="POST">
        {{csrf_field()}}
        {{method_field('PUT')}}
        <div class="form-group">
            <label for="name">Username :</label>
            <input type="text" class="form-control" placeholder="Enter username" name="username" id="username" value="{{$users -> username}}">
        </div>
        <div class="form-group">
            <label for="name">Password :</label>
            <input type="text" class="form-control" placeholder="Enter password" name="password" id="password" value="{{$users -> password}}">
        </div>
        <div class="form-group">
            <label for="price">Role :</label>
            <input type="text" class="form-control" placeholder="Enter user's role" name="role" id="role" value="{{$users -> role}}">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Update</button>
            <a class="btn btn-danger" href="/admin/user" role="button">Cancel</a>
        </div>
    </form>
@endsection